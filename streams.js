// I want to model the following flow

// read block of audio -> extract features -> format the features in some way

// it may be possible to model this with streams, either Node.js streams, 
// something similar with EventEmitter, or RxJs.Observables

// it is possible that the audio is either pushed in, with no control over the rate, 
// or that it is pulled from. For simplicity, I think it is easier to pull it.

const { Readable, Transform } = require('stream');
const { loadAndConfigure } = require('piper/HigherLevelUtilities');
const { PiperClient, FeatsService, EmscriptenProxy, fromFrames } = require('piper');
const vampExamples = require('piper/ext/VampExamplePlugins');
const deinterleave = require('./deinterleave');

class StubAudioStream extends Readable {
  constructor(interleaved, channelCount, blockSize, stepSize) {
    super();
    this.interleaved = interleaved;
    this.channelCount = channelCount;
    this.blockSize = blockSize * channelCount;
    this.stepSize = stepSize * channelCount;
    this.readPointer = 0;
  }
  
  _read() {
    const block = this.interleaved.subarray(this.readPointer, this.readPointer + this.blockSize);
    const toBuffer = a => Buffer.from(a.buffer, a.byteOffset, a.byteLength);
    this.readPointer += this.stepSize;
    if (block.length === this.blockSize)
      this.push(toBuffer(block));
    else if (block.length > 0)
      this.push(toBuffer(Float32Array.of(...block, ...new Float32Array(this.blockSize - block.length))));
    else 
      this.push(null);
  }
}

class ProcessTransform extends Transform {
  constructor(service, simpleRequest) {
    super();
    this.previous = new Float32Array(4);
    this.service = (service instanceof PiperClient) 
                ? service 
                : new PiperClient(service); // probably not wise to assume this'll work
    this.frameCount = 0;
    this.request = simpleRequest;
    this.simpleConfig = loadAndConfigure(simpleRequest, this.service); // probably problematic that async work which could fail later happens in the constructor. 
  }
  
  _transform(chunk, encoding, callback) {
    const floats = new Float32Array(chunk.buffer, chunk.byteOffset, Float32Array.BYTES_PER_ELEMENT);
    const toBuffer = a => Buffer.from(a.buffer, a.byteOffset, a.byteLength);
    // const doTransform = () => {
    //   this.push(toBuffer(floats.map(val => val * this.previous[0])));
    //   this.previous.set(floats);
    //   callback();
    // };
    // setTimeout(doTransform, 100 / Math.random());
    this.simpleConfig.then(config => {
      return this.service.process({
        handle: config.handle,
        processInput: {
          timestamp: fromFrames(this.frameCount++, this.request.audioFormat.sampleRate),
          inputBuffers: deinterleave({
            data: floats,
            format: this.request.audioFormat
          }).data
        }
      })
      .then(processResponse => {
        this.push(JSON.stringify(processResponse));
        callback();
      })
      .catch(error => { console.error(error)}); // TODO in a lot of circumstances, if this wasn't wrapped in a PiperClient, the response would already be a string. This could be desirable.
    }).catch(error => { console.error(error)});;
    // TODO what about errors?
  }
}

module.exports['StubAudioStream'] = StubAudioStream;
const audioData = new Float32Array([...Array(44100).keys()]);
const request = {
  audioData: audioData,
  audioFormat: {
    channelCount: 1,
    sampleRate: 16
  },
  key: 'vamp-example-plugins:zerocrossing'
};
const client = new PiperClient(new EmscriptenProxy(vampExamples()));
const blockSize = 1024;
const stepSize = 1024;

var a = new StubAudioStream(
  audioData,
  1,
  blockSize,
  stepSize
);

var b = new ProcessTransform(client, request);
const extractor = a.pipe(b);
extractor.on('data', chunk => console.log(chunk.toString()));
extractor.on('end', () => console.log('should call finish'));
// a.pipe(process.stdout);