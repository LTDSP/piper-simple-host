const spawn = require('child_process').spawn;
const Transform = require('stream').Transform;
const split = require('split');
const os = require('os');
const { Serialise, Deserialise } = require('piper/JsonProtocol');

class ChildProcessPiperService {
  constructor(pathToServer) {
    this.piper = spawn(
      pathToServer,
      ['json']
    );
    
    // TODO figure out why output stops sometimes unless stderr is consumed
    this.piper.stderr.on('data', (err) => {});
    
    const endLineTerminatedStream = this.piper.stdout.pipe(split());
    this.spawnedService = (rpcMessageString) => {
      return new Promise(resolve => {
        
        const responseListener = (rpcResponseString) => { 
          endLineTerminatedStream.removeListener('data', responseListener);
          resolve(rpcResponseString);
        };
        
        endLineTerminatedStream
        .on('data', responseListener);
        this.piper.stdin.write(rpcMessageString + os.EOL);
      });
    }
  }
  
  list(request) {
    return this.spawnedService(Serialise.ListRequest(request))
    .then(Deserialise.ListResponse);
  }
  
  load(request) {
    return this.spawnedService(Serialise.LoadRequest(request))
    .then(Deserialise.LoadResponse);
  }
  
  configure(request) {
    return this.spawnedService(Serialise.ConfigurationRequest(request))
    .then(Deserialise.ConfigurationResponse);
  }
  
  process(request) {
    return this.spawnedService(Serialise.ProcessRequest(request))
    .then(Deserialise.ProcessResponse);
  }
  
  finish(request) {
    return this.spawnedService(Serialise.FinishRequest(request))
    .then(Deserialise.FinishResponse);
  }

  _close() {
    this.piper.kill();
  }
}

module.exports = ChildProcessPiperService;
