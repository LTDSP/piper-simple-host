#!/usr/bin/env node
const Piper = require('piper');
const EmscriptenProxy = require('piper/EmscriptenProxy').EmscriptenProxy; 
const decodeAudioFile = require('./decodeAudioFile');
const PiperSimpleClient = require('piper/HigherLevelUtilities').PiperSimpleClient;
const createServerFromFilename = require('./PiperServerFactory').createServerFromFilename;
const ChildProcessPiperService = require('./ChildProcessPiperService'); 

const isListRequest = process.argv.includes("--list-outputs") && process.argv.length === 4;
if (!isListRequest && process.argv.length < 4 || process.argv.length > 5) {
  console.log("\nUsage: " + process.argv[0] + " <librarypath> <audiofile> [<pluginkey>]");
  console.log("e.g. " + process.argv[0] + " ./VampExamplePlugins.js ./testsignal.wav");
  console.log("e.g. " + process.argv[0] + " ./VampExamplePlugins.js ./testsignal.wav vamp-example-plugins:zerocrossing");
  throw "Wrong number of command-line args (2 or 3 expected)"
}

const extractorLibraryPath = process.argv[2]; 
const filename = process.argv[3];
let extractorKey = process.argv[4] || "";
let outputId = "";
const keyParts = extractorKey.split(":")
if (keyParts.length > 2) {
  extractorKey = `${keyParts[0]}:${keyParts[1]}`;
  outputId = keyParts[2];
}

const piperServer = createServerFromFilename(extractorLibraryPath);
const piperClient = new PiperSimpleClient(piperServer);

function closeConnection(server) {
  if (server instanceof ChildProcessPiperService) 
    server._close();
}

// ---- main
if (isListRequest) {
  piperClient
  .list({})
  .then(res => res.available)
  .then(listOutputs)
  .then(() => closeConnection(piperServer))
  .catch(console.error);    
}
else {
  decodeAudioFile(filename)
  .then(extractFeatures)
  .then(simplePrint)
  .then(() => closeConnection(piperServer))
  .catch(console.error);    
}

// ---- function definitions

function listOutputs(available) {
  for (let staticData of available) {
    for (let outputInfo of staticData.basicOutputInfo)
      console.log(`${staticData.key}:${outputInfo.identifier}`)
  }
}

function getOutputIdToStaticDataMap(available) {
  return new Map(available.map(output => [output.basic.identifier, output]));
}

function extractFeatures(audioData) {
  console.log('extracting features..');
  return piperClient.process({
    audioData: audioData.data,
    audioFormat: audioData.format,
    key: extractorKey,
    outputId: outputId
  });
}

function toLines(features) {
  return features.map(feature => {
    const formatTimestamp = (timestamp) => Piper.toSeconds(timestamp).toFixed(9);
    const timestampSecs = formatTimestamp(feature.timestamp);
    const duration = feature.duration ? `, ${formatTimestamp(feature.duration)}` : '';
    const delimited = [...feature.featureValues].map(val => 1 * val.toFixed(7)).join(' ');
    const label = feature.label || '';
    return ` ${timestampSecs}${duration}: ${delimited} ${label}`;
  });
}

function simplePrint(features) {
  for (let line of toLines([...features]))
    console.log(line);
}

