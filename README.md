# piper-simple-host

This is a simple example of a Piper audio feature extractor host application, providing an interface similar to the vamp-simple-host from the [Vamp Plugin SDK](https://code.soundsoftware.ac.uk/projects/vamp-plugin-sdk). 

It is currently limited, and mainly exists as the simplest possible use case in which to explore how the current tools provided in piper-js fit together.

To use:

Clone the repo, and then run `npm install` (or `yarn`).

Currently .wav and .mp3 files are support, and vamp plugins adapted to a Piper server using the [Piper Vamp JSON Adapter](https://code.soundsoftware.ac.uk/projects/piper-vamp-js).

```bash
./host.js ./testsignal.wav ./node_modules/piper/ext/VampTestPlugin.js vamp-test-plugin:vamp-test-plugin:grid-oss
```