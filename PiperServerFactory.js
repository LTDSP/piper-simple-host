const path = require('path');
const ChildProcessPiperService = require('./ChildProcessPiperService');
const EmscriptenProxy = require('piper/EmscriptenProxy').EmscriptenProxy;

// crude way of deciding which server to instantiate, mainly based on the filename
function createServerFromFilename(uri) {
  const extension = path.extname(uri);
  switch (extension) {
  case '.js':
    return determineJsType(uri);
  case '':
    return new ChildProcessPiperService(uri); // assume child process  
  default:
    throw new Error("Could not instantiate a valid server.");
  }
}

function determineJsType(uri) {
  // TODO what about FeatsService?
  // for now just assume emscripten
  return new EmscriptenProxy(require(uri)());
}

module.exports['createServerFromFilename'] = createServerFromFilename;