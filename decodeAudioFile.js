const AV = require('av');
const deinterleave = require('./deinterleave');
require('mp3');


function decodeAudioFile(filename) {
  return new Promise((res, rej) => {
    const asset = AV.Asset.fromFile(filename); 
    asset.get('format', (format) => {
      console.log('Decoding audio..');
      asset.decodeToBuffer((buffer) => { 
        res({
          data: buffer, 
          format: {
            channelCount: format.channelsPerFrame,
            sampleRate: format.sampleRate
          }
        });
      });
    });
    asset.on('error', (err) => rej(err));
  });
}

module.exports = (filename) => decodeAudioFile(filename)
.then((audio) => {
  console.log('deinterleaving..');
  return deinterleave(audio);
});