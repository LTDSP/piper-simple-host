function deinterleave(audio) {
  let deinterleaved = [];
  const durationSamples = audio.data.length / audio.format.channelCount;
  
  for (let channel = 0; channel < audio.format.channelCount; ++channel) {
    deinterleaved.push(new Float32Array(durationSamples));
  }
  
  for (let n = 0, i = 0; n < audio.data.length; n += audio.format.channelCount, ++i) {
    for (let channel = 0; channel < audio.format.channelCount; ++channel) {
      deinterleaved[channel][i] = audio.data[n + channel];
    }
  }
  return {
    data: deinterleaved,
    format: audio.format
  };
}

module.exports = deinterleave;